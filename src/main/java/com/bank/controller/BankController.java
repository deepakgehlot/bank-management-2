package com.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.model.BankAccount;
import com.bank.service.BankService;

@RestController
@RequestMapping("/xyz")
public class BankController {
	@Autowired
	private BankService bank_ser;
	
	
	@GetMapping("/get")
	public ResponseEntity<List<BankAccount>> getAll()
	{
		return ResponseEntity.ok(bank_ser.findAll());
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<BankAccount> getAllById(@PathVariable Long id)
	{
		return ResponseEntity.ok(bank_ser.findById((long) id).orElse(null));
	}
	
	@PostMapping("/add")
	public ResponseEntity<BankAccount> addAll(@RequestBody BankAccount bacc)
	{
		return ResponseEntity.ok(bank_ser.save(bacc));
		
	}
	
	@PutMapping("/update")
	public ResponseEntity<BankAccount> updateAll(@RequestBody BankAccount bacc)
	{
		return ResponseEntity.ok(bank_ser.save(bacc));
		
	}

}
