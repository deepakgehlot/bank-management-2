package com.bank.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BankAccount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int accNo;
	private String name;
	private String pancard;
	private long aadhar;
	private String address;

	

	public BankAccount() {
		super();
		// TODO Auto-generated constructor stub
	}


	public BankAccount(int accNo, String name, String pancard, long aadhar, String address) {
		super();
		this.accNo = accNo;
		this.name = name;
		this.pancard = pancard;
		this.aadhar = aadhar;
		this.address = address;
	}


	public int getAccNo() {
		return accNo;
	}


	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPancard() {
		return pancard;
	}


	public void setPancard(String pancard) {
		this.pancard = pancard;
	}


	public long getAadhar() {
		return aadhar;
	}


	public void setAadhar(long aadhar) {
		this.aadhar = aadhar;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}
	
}
