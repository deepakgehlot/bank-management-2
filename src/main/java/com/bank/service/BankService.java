package com.bank.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.model.BankAccount;

public interface BankService  extends JpaRepository<BankAccount, Long>  {

}
